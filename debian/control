Source: python-morris
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Zygmunt Krynicki <zygmunt.krynicki@canonical.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.6.2.1
Vcs-Git: https://salsa.debian.org/python-team/packages/python-morris.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-morris
Homepage: https://github.com/zyga/morris
Rules-Requires-Root: no

Package: python3-morris
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: announcement (signal/event) system for Python 3
 Morris is a simple Python library for creating notification mechanism similar
 to Qt signals or C# events. Application developers can create signals with a
 simple decorator (@signal), send signals by calling the decorated method or
 function, connect to and disconnect from signals with signal.connect() and
 signal.disconnect().
 .
 Morris comes with support for writing high-level unit tests using the
 SignalTestCase.{watchSignal,assertSignalFired,assertSignalNotFired}() methods.
 Appropriate ordering constraints on multiple signals can be tested using the
 SignalTestCase.assertSignalOrdering() method.
 .
 This package contains version of the library for Python 3

Package: python-morris-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: documentation for the Python morris module
 Morris is a simple Python library for creating notification mechanism similar
 to Qt signals or C# events. Application developers can create signals with a
 simple decorator (@signal), send signals by calling the decorated method or
 function, connect to and disconnect from signals with signal.connect() and
 signal.disconnect().
 .
 Morris comes with support for writing high-level unit tests using the
 SignalTestCase.{watchSignal,assertSignalFired,assertSignalNotFired}() methods.
 Appropriate ordering constraints on multiple signals can be tested using the
 SignalTestCase.assertSignalOrdering() method.
 .
 This package contains the HTML documentation
